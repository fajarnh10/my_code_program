package com.example.service.module;

import java.util.Collections;
import java.util.HashMap;
import java.util.Objects;

import com.example.model.ParkingModel;
import com.example.util.ParkingUtils;

public class ParkingModule {
	
	public static ParkingModel create_parking_lot(Integer length, ParkingModel parkingModel) {
		try {
			if (parkingModel.getLotCapacity() != null && parkingModel.getLotCapacity() > 0) {
				System.out.println("Parking lot is ready in "+ parkingModel.getLotCapacity() +" slots");
			}else {
				if (length != null && length > 0) {
					parkingModel.setLotCapacity(length);
					parkingModel.setFreeSpot(ParkingUtils.createFreeSpot(length));
					System.out.println("Create parking lot with " + length + " slots");
				}else {
					System.out.println("Invalid Size Of Lot !");
				}
			}
		} catch (Exception e) {
			System.out.println("Error when create parking lot \n" + e);
		}
		
		return parkingModel;
	}

	public static ParkingModel park(String plateNumb, ParkingModel parkingModel) {
		try {
			if (parkingModel.getLotCapacity() != null && parkingModel.getLotCapacity() > 0) {
				if (parkingModel.getLotValue() != null && parkingModel.getLotValue().size() == parkingModel.getLotCapacity()) {
					System.out.println("Sorry, parking lot is full");
				}else {
					if (plateNumb != null && !plateNumb.isEmpty()) {
						boolean isReadyIn = false;
						if (parkingModel.getLotValue() != null) {
							String findPlate = parkingModel.getLotValue().entrySet().stream().filter(entry -> Objects.equals(entry.getValue(), plateNumb)).map(HashMap.Entry::getKey).findFirst().toString().replaceAll("[^\\d]", "");
							if (findPlate != null && !findPlate.isEmpty()) {
								isReadyIn = true;
							}
						}
						
						if (isReadyIn) {
							System.out.println("Plate Number Already In !");
						}else {
							
							HashMap<Integer, String> lotList = new HashMap<Integer, String>();
							
							if (parkingModel.getLotValue() != null) {
								lotList = new HashMap<Integer, String>(parkingModel.getLotValue());
							}
							
							String freeSpot = String.valueOf(parkingModel.getFreeSpot().get(0));
							parkingModel.getFreeSpot().remove(0);
							
							lotList.put(Integer.parseInt(freeSpot), plateNumb);
							
							parkingModel.setLotValue(lotList);
							
							System.out.println("Allocated slot number: " + freeSpot);
						}
					}else {
						System.out.println("Plate Number Cannot Be Empty !");
					}
				}
			}else {
				System.out.println("Please create parking lot first !");
			}
		} catch (Exception e) {
			System.out.println("Error when parking : " + e);
		}
		
		return parkingModel;
	}
	
	public ParkingModel leave(String plateNumber, String charge, ParkingModel parkingModel) {
		try {
			if (parkingModel.getLotCapacity() != null && parkingModel.getLotCapacity() > 0) {
				if (parkingModel.getLotValue() != null && parkingModel.getLotValue().size() > 0) {
					boolean notFound = true;
					String findPlate = parkingModel.getLotValue().entrySet().stream().filter(entry -> Objects.equals(entry.getValue(), plateNumber)).map(HashMap.Entry::getKey).findFirst().toString().replaceAll("[^\\d]", "");
					if (findPlate != null && !findPlate.isEmpty()) {
						System.out.println("Registration number " + plateNumber + " with slot Number " + findPlate + " is free with Charge " + ParkingUtils.calculateCharge(charge));
						parkingModel.getFreeSpot().add(Integer.parseInt(findPlate));
						Collections.sort(parkingModel.getFreeSpot());
						parkingModel.getLotValue().remove(Integer.parseInt(findPlate));
						notFound = false;
					}
					
					if (notFound) {
						System.out.println("Registration number "+plateNumber+" not found");
					}
				}else {
					System.out.println("No cars were parked at this time !");
				}
			}else {
				System.out.println("Please create parking lot first !");
			}
		} catch (Exception e) {
			System.out.println("Error when leaving car : " + e);
		}
		return parkingModel;
	}
	
	public ParkingModel status(ParkingModel parkingModel) {
		System.out.println("Slot No. Registration No.");
		
		if (parkingModel.getLotValue() != null) {
			parkingModel.getLotValue().forEach((K,V) -> System.out.println(String.valueOf(K).replace(",", "") + (" " + V)));
		}
		
		return parkingModel;
	}
}
