package com.example.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.example.constant.ServiceHelper;
import com.example.model.ParkingModel;
import com.example.service.module.ParkingModule;
import com.example.util.ParkingUtils;

public class ParkingApplication {
	
	public static ParkingModel parkingMenu(String command, ParkingModel parkingModel) {

		ParkingModule parkingModule = new ParkingModule();
		ParkingUtils parkingUtils = new ParkingUtils();
		
		if (command != null && !command.isEmpty()) {
			if (command.contains(ServiceHelper.CONST_COMMAND_CREATE_PARKING_LOT)) {
				String strSizeLot = command.substring(ServiceHelper.CONST_COMMAND_CREATE_PARKING_LOT.length(), command.length()).replace(" ", "");
				if (parkingUtils.validateNumericFormat(strSizeLot)) {
					Integer sizeLot = Integer.parseInt(strSizeLot);
					parkingModel = ParkingModule.create_parking_lot(sizeLot, parkingModel);
				}else {
					System.out.println("Invalid Size Of Lot !");
				}
			}else if (command.contains(ServiceHelper.CONST_COMMAND_PARK)) {
				String plateNumb = command.substring(ServiceHelper.CONST_COMMAND_PARK.length(), command.length()).replace(" ", "");
				parkingModel = ParkingModule.park(plateNumb, parkingModel);
			}else if (command.contains(ServiceHelper.CONST_COMMAND_LEAVE)) {
				List<String> commandList = new ArrayList<String>(Arrays.asList(command.split(" ")));
				if (commandList != null && commandList.size() == 3) {
					String plateNumb = commandList.get(1);
					String charge = commandList.get(2);
					if (parkingUtils.validateNumericFormat(charge)) {
						parkingModel = parkingModule.leave(plateNumb, charge, parkingModel);
					}else {
						System.out.println("Invalid Charge Format !");
					}
				}else {
					System.out.println("Invalid Leave command !");
				}
			}else if (command.contains(ServiceHelper.CONST_COMMAND_STATUS)) {
				parkingModel = parkingModule.status(parkingModel);
			}else {
				System.out.println("Incomprehensible command !");
			}
		}else {
			System.out.println("Command not found !");
		}
		
		return parkingModel;
	}
}
