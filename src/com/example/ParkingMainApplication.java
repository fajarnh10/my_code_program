package com.example;

import java.util.Scanner;

import com.example.model.ParkingModel;
import com.example.service.ParkingApplication;

public class ParkingMainApplication {
	public static void main(String[] args) {
		ParkingModel parkingModel = new ParkingModel();
		
		System.out.println("Welcome To Parking Application, Write Command Bellow to Using Application");
		
		while (true) {
			try {
				@SuppressWarnings("resource")
				Scanner input = new Scanner(System.in);
				String command = input.nextLine();
				
				parkingModel = ParkingApplication.parkingMenu(command, parkingModel);
			} catch (Exception e) {
				System.out.println("Something Error When Running Parking Application " + e);
			}
		}
	}
}
