package com.example.testing;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.example.constant.ServiceHelper;
import com.example.model.ParkingModel;
import com.example.service.ParkingApplication;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ParkingUnitTesting {
	
	@Test
	void create_parking_lot_6() throws JsonProcessingException {
		ParkingModel parkingModel = new ParkingModel();
		List<Integer> freeSpot = new ArrayList<Integer>();
		parkingModel.setLotCapacity(6);
		for (int i = 0; i < parkingModel.getLotCapacity(); i++) {
			freeSpot.add(i+1);
		}
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_CREATE_PARKING_LOT + " 6", new ParkingModel()));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse);
	}
	
	@Test
	void park_KA_01_HH_1234() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-01-HH-1234");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 1; i < lotCapacity; i++) {
			freeSpot.add(i+1);
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[1,2,3,4,5,6],\"lotValue\":null}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " KA-01-HH-1234" , parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void park_KA_01_HH_9999() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-01-HH-1234");
		lotValue.put(2, "KA-01-HH-9999");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 2; i < lotCapacity; i++) {
			freeSpot.add(i+1);
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[2,3,4,5,6],\"lotValue\":{\"1\":\"KA-01-HH-1234\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " KA-01-HH-9999" , parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void park_KA_01_BB_0001() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-01-HH-1234");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "KA-01-BB-0001");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 3; i < lotCapacity; i++) {
			freeSpot.add(i+1);
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[3,4,5,6],\"lotValue\":{\"1\":\"KA-01-HH-1234\",\"2\":\"KA-01-HH-9999\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " KA-01-BB-0001" , parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void park_KA_01_HH_7777() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-01-HH-1234");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "KA-01-BB-0001");
		lotValue.put(4, "KA-01-HH-7777");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 4; i < lotCapacity; i++) {
			freeSpot.add(i+1);
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[4,5,6],\"lotValue\":{\"1\":\"KA-01-HH-1234\",\"2\":\"KA-01-HH-9999\",\"3\":\"KA-01-BB-0001\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " KA-01-HH-7777" , parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void park_KA_01_HH_2701() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-01-HH-1234");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "KA-01-BB-0001");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 5; i < lotCapacity; i++) {
			freeSpot.add(i+1);
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[5,6],\"lotValue\":{\"1\":\"KA-01-HH-1234\",\"2\":\"KA-01-HH-9999\",\"3\":\"KA-01-BB-0001\",\"4\":\"KA-01-HH-7777\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " KA-01-HH-2701" , parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void park_KA_01_HH_3141() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-01-HH-1234");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "KA-01-BB-0001");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		lotValue.put(6, "KA-01-HH-3141");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 6; i < lotCapacity; i++) {
			freeSpot.add(i+1);
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[6],\"lotValue\":{\"1\":\"KA-01-HH-1234\",\"2\":\"KA-01-HH-9999\",\"3\":\"KA-01-BB-0001\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " KA-01-HH-3141" , parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void leave_KA_01_HH_3141_4() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-01-HH-1234");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "KA-01-BB-0001");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 0; i < lotCapacity; i++) {
			if (i == 5) {
				freeSpot.add(i+1);
			}
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[],\"lotValue\":{\"1\":\"KA-01-HH-1234\",\"2\":\"KA-01-HH-9999\",\"3\":\"KA-01-BB-0001\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\",\"6\":\"KA-01-HH-3141\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_LEAVE + " KA-01-HH-3141 4" , parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void status1() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-01-HH-1234");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "KA-01-BB-0001");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 0; i < lotCapacity; i++) {
			if (i == 5) {
				freeSpot.add(i+1);
			}
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[6],\"lotValue\":{\"1\":\"KA-01-HH-1234\",\"2\":\"KA-01-HH-9999\",\"3\":\"KA-01-BB-0001\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_STATUS, parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void park_KA_01_P_333() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-01-HH-1234");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "KA-01-BB-0001");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		lotValue.put(6, "KA-01-P-333");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 0; i < lotCapacity; i++) {
			if (i == 6) {
				freeSpot.add(i+1);
			}
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[6],\"lotValue\":{\"1\":\"KA-01-HH-1234\",\"2\":\"KA-01-HH-9999\",\"3\":\"KA-01-BB-0001\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " KA-01-P-333", parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void park_DL_12_AA_9999() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-01-HH-1234");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "KA-01-BB-0001");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		lotValue.put(6, "KA-01-P-333");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 0; i < lotCapacity; i++) {
			if (i == 6) {
				freeSpot.add(i+1);
			}
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[],\"lotValue\":{\"1\":\"KA-01-HH-1234\",\"2\":\"KA-01-HH-9999\",\"3\":\"KA-01-BB-0001\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\",\"6\":\"KA-01-P-333\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " DL-12-AA-9999", parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void leave_KA_01_HH_1234_4() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "KA-01-BB-0001");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		lotValue.put(6, "KA-01-P-333");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 0; i < lotCapacity; i++) {
			if (i == 0) {
				freeSpot.add(i+1);
			}
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[],\"lotValue\":{\"1\":\"KA-01-HH-1234\",\"2\":\"KA-01-HH-9999\",\"3\":\"KA-01-BB-0001\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\",\"6\":\"KA-01-P-333\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_LEAVE + " KA-01-HH-1234 4", parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void leave_KA_01_BB_0001_6() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		lotValue.put(6, "KA-01-P-333");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 0; i < lotCapacity; i++) {
			if (i == 0 || i == 2) {
				freeSpot.add(i+1);
			}
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[1],\"lotValue\":{\"2\":\"KA-01-HH-9999\",\"3\":\"KA-01-BB-0001\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\",\"6\":\"KA-01-P-333\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_LEAVE + " KA-01-BB-0001 6", parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void leave_KA_12_AA_9999_2() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		lotValue.put(6, "KA-01-P-333");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 0; i < lotCapacity; i++) {
			if (i == 0 || i == 2) {
				freeSpot.add(i+1);
			}
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[1,3],\"lotValue\":{\"2\":\"KA-01-HH-9999\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\",\"6\":\"KA-01-P-333\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_LEAVE + " DL-12-AA-9999 2", parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void park_KA_09_HH_0987() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-09-HH-0987");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		lotValue.put(6, "KA-01-P-333");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 0; i < lotCapacity; i++) {
			if (i == 2) {
				freeSpot.add(i+1);
			}
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[1,3],\"lotValue\":{\"2\":\"KA-01-HH-9999\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\",\"6\":\"KA-01-P-333\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " KA-09-HH-0987", parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void park_CA_09_IO_1111() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-09-HH-0987");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "CA-09-IO-1111");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		lotValue.put(6, "KA-01-P-333");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 6; i < lotCapacity; i++) {
			freeSpot.add(i+1);
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[3],\"lotValue\":{\"1\":\"KA-09-HH-0987\",\"2\":\"KA-01-HH-9999\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\",\"6\":\"KA-01-P-333\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " CA-09-IO-1111", parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void park_KA_09_HH_0123() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-09-HH-0987");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "CA-09-IO-1111");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		lotValue.put(6, "KA-01-P-333");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 6; i < lotCapacity; i++) {
			freeSpot.add(i+1);
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[],\"lotValue\":{\"1\":\"KA-09-HH-0987\",\"2\":\"KA-01-HH-9999\",\"3\":\"CA-09-IO-1111\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\",\"6\":\"KA-01-P-333\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_PARK + " KA-09-HH-0123", parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
	
	@Test
	void status() throws JsonProcessingException {
		
		Integer lotCapacity = 6;
		
		HashMap<Integer, String> lotValue = new HashMap<Integer, String>();
		lotValue.put(1, "KA-09-HH-0987");
		lotValue.put(2, "KA-01-HH-9999");
		lotValue.put(3, "CA-09-IO-1111");
		lotValue.put(4, "KA-01-HH-7777");
		lotValue.put(5, "KA-01-HH-2701");
		lotValue.put(6, "KA-01-P-333");
		
		List<Integer> freeSpot = new ArrayList<Integer>();
		for (int i = 6; i < lotCapacity; i++) {
			freeSpot.add(i+1);
		}
		
		ParkingModel parkingModel = new ParkingModel();
		parkingModel.setLotCapacity(lotCapacity);
		parkingModel.setLotValue(lotValue);
		parkingModel.setFreeSpot(freeSpot);
		
		ObjectMapper mapper = new ObjectMapper();
		String updatedData = "{\"lotCapacity\":6,\"freeSpot\":[],\"lotValue\":{\"1\":\"KA-09-HH-0987\",\"2\":\"KA-01-HH-9999\",\"3\":\"CA-09-IO-1111\",\"4\":\"KA-01-HH-7777\",\"5\":\"KA-01-HH-2701\",\"6\":\"KA-01-P-333\"}}";
		ParkingModel parkingModelRequest = mapper.readValue(updatedData, ParkingModel.class);
		String parkingModelResponse = mapper.writeValueAsString(ParkingApplication
				.parkingMenu(ServiceHelper.CONST_COMMAND_STATUS, parkingModelRequest));
		assertEquals(mapper.writeValueAsString(parkingModel), parkingModelResponse );
	}
}
