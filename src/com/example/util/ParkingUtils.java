package com.example.util;

import java.util.ArrayList;
import java.util.List;

public class ParkingUtils {

	public boolean validateNumericFormat(String string) {
		boolean returnValue = false;
		try {
			Integer.parseInt(string);
			returnValue = true;
		} catch (Exception e) {
			System.out.println("Is Not Numeric Format !");
		}
		return returnValue;
	}
	
	public static List<Integer> createFreeSpot(Integer lotCapacity){
		List<Integer> returnValue = new ArrayList<Integer>();
		for (int i = 0; i < lotCapacity; i++) {
			returnValue.add(i+1);
		}
		return returnValue;
	}
	
	public static String calculateCharge(String time) {
		int intTime = Integer.parseInt(time);
		String returnValue = null;
		if (intTime > 0 && intTime <= 2) {
			returnValue = "10";
		}else if (intTime > 2) {
			returnValue = String.valueOf(10+((intTime-2)*10));
		}else{
			System.out.println("Invalid Charge !");
		}
		return returnValue;
	}
}
