package com.example.model;

import java.util.HashMap;
import java.util.List;

public class ParkingModel {

	private Integer lotCapacity;
	
	private List<Integer> freeSpot;
	
	private HashMap<Integer, String> lotValue;

	public Integer getLotCapacity() {
		return lotCapacity;
	}

	public void setLotCapacity(Integer lotCapacity) {
		this.lotCapacity = lotCapacity;
	}
	
	public List<Integer> getFreeSpot() {
		return freeSpot;
	}

	public void setFreeSpot(List<Integer> freeSpot) {
		this.freeSpot = freeSpot;
	}

	public HashMap<Integer, String> getLotValue() {
		return lotValue;
	}

	public void setLotValue(HashMap<Integer, String> lotValue) {
		this.lotValue = lotValue;
	}
}
