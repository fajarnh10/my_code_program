package com.example.constant;

public class ServiceHelper {
	public static final String CONST_COMMAND_CREATE_PARKING_LOT 	= "create_parking_lot";
	public static final String CONST_COMMAND_PARK 					= "park";
	public static final String CONST_COMMAND_LEAVE 					= "leave";
	public static final String CONST_COMMAND_STATUS 				= "status";
}
