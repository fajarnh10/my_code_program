# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Version 1.0.0
* [Parking Console](https://bitbucket.org/fajarnh10/my_code_program)

### Get set up ###

* Export ParkingMainApplication to jar files
* Make sure java8 already installed in your computer
* Run jar file with command java -jar (Name jar file).jar

### Application command ###

* create_parking_lot (Size of parking lot in integer character) 
This command usable for create parking lot size

* park (ticket number)
You can use this command for save ticket number to application

* leave (ticket number) (time)
When leaving the parking lot, this command should be use to remove your ticket data and new customers can parking in parking lot later

* status
Status command used for showing data, how many parking lot filled and how many parking lot is empty

### Apps Note ###
* To use this apps, create_parking_lot is mandatory command to use in the first your write command.
* The customer will be allocated a parking slot which is nearest to the entry.
* Charge applicable is $10 for first 2 hours and $10 for every additional hour.
* When the parking lot full, plate number will be not issued.